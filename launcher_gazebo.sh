#!/bin/bash

DRONE_SWARM_MEMBERS=$1
export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects/multidrone_bridge_inspection_rotors_simulator
if [ -z $DRONE_SWARM_MEMBERS ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting Swarm Members = 2"
    	DRONE_SWARM_MEMBERS=2
  else
    	echo "-Setting DroneSwarm Members = $1"
fi

gnome-terminal  \
   	--tab --title "DroneRotorsSimulator" --command "bash -c \"
roslaunch ${AEROSTACK_PROJECT}/rotors_files/launch/suspension_bridge.launch --wait project:=${AEROSTACK_PROJECT};
						exec bash\""  &
sleep 5
for (( c=1; c<=$DRONE_SWARM_MEMBERS; c++ ))
do  
gnome-terminal  \
   	--tab --title "DroneRotorsSimulator" --command "bash -c \"
roslaunch ${AEROSTACK_PROJECT}/rotors_files/mav_swarm$c.launch --wait drone_swarm_number:=$c mav_name:=hummingbird;
						exec bash\""  &
done


