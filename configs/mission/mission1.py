#!/usr/bin/env python3

import mission_execution_control as mxc
import rospy

def followPath(ruta):
  for point in ruta:
    goToPoint(point)

def goToPoint(point):
  while True:
    result, activation_succesful = mxc.executeTask('FOLLOW_PATH', path=[point])
    if(result != 5 and activation_succesful):# result=5 means INTERRUPTED
      break



def mission():
  print("Starting mission...")

  print("Paying attention to robots...")
  mxc.startTask('PAY_ATTENTION_TO_ROBOT_MESSAGES')

  print("informing position to robots...")
  mxc.startTask('INFORM_POSITION_TO_ROBOTS')

  print("Taking off...")
  mxc.executeTask('TAKE_OFF')

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', destination="drone112", text="drone111_start_mission")

  print("Following path")
  followPath([ [-1, -5, 2.3],[-1, 0, 2.3] ])

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', destination="drone112", text="drone111_inspect_the_first_tower")

  print("Following path")
  followPath([ [-1, 0, 6.5] ])

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', destination="drone112", text="drone111_over_the_top_of_the_tower")

  print("Following path")
  followPath([ [-1, 0, 2.3]  ])

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', destination="drone112", text="drone111_doesnt_find_problems")


  print("Following path")
  followPath([ [-1, 4, 3]])

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', destination="drone112", text="drone111_is_going_to_cross_the_bridge")

  print("Following path")
  followPath([ [-3, 5.5, 3]])

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', destination="drone112", text="drone111_doesnt_found_problems_on_the_bridge")

  print("Following path")
  followPath([ [-5, 7, 3],[-5, 8, 2.3]])

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', destination="drone112", text="drone111_is_on_the_other_side_of_the_bridge")

  print("Following path")
  followPath([ [-5, 12, 2.3]])

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', destination="drone112", text="drone111_inspect_the_second_tower")

  print("Following path")
  followPath([ [-5, 12, 6.5]])

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', destination="drone112", text="drone111_over_the_top_of_the_tower")

  print("Following path")
  followPath([ [-5, 12, 2.3], [-5,17,2.3]])

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', destination="drone112", text="drone111_mission_completed")

  print("Landing")
  mxc.executeTask('LAND')

  print('Finishing mission...')
