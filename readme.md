
## Multidrone bridge inspection with Rotors simulator

In order to execute the mission defined in configs/mission/behavior_tree_mission_file.yaml, perform the following steps:

- Execute the roscore:

        $ roscore

- Execute the script that launches Gazebo for this project:

        $ ./launcher_gazebo.sh

![gazebo_launched.png](https://i.ibb.co/v4t0Zs7/gazebo.png)

- Execute the script that launches the Aerostack components for this project:

        $ ./main_launcher.sh
##![main_launcherCamera.png](https://i.ibb.co/khDYVJd/camera.png)
##![main_launcherTree.png](https://i.ibb.co/MBH2MdC/tree.png)

##Here there is a video that shows how to launch the project:

##[ ![Launch project](https://img.youtube.com/vi/5Tza9svcE6Q/0.jpg)](https://www.youtube.com/watch?v=5Tza9svcE6Q)

##Here there is a video that shows the correct execution of the mission:

##[ ![Rotors Simulator Mission](https://img.youtube.com/vi/1sX4oj02Muw/0.jpg)](https://www.youtube.com/watch?v=1sX4oj02Muw)

